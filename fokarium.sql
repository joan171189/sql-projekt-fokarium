
CREATE TABLE zwierze (
  imie VARCHAR(10) PRIMARY KEY,
  gatunek VARCHAR(10),
  koszt numeric(4)
);

CREATE TABLE sponsor (
  id VARCHAR(10) PRIMARY KEY, 
  ulubiony VARCHAR(10)
);

CREATE TABLE datek (
  kto VARCHAR(10) REFERENCES sponsor,
  komu VARCHAR(10) REFERENCES zwierze,
  ile numeric(4),
  CONSTRAINT datek_pk PRIMARY KEY(kto, komu)
);

INSERT INTO zwierze VALUES ('Gudrun', 'mors', 25);
INSERT INTO zwierze VALUES ('Gerald', 'mors', 20);
INSERT INTO zwierze VALUES ('Pepe', 'uchatka', 25);
INSERT INTO zwierze VALUES ('Dolores', 'uchatka', 20);
INSERT INTO zwierze VALUES ('Masza', 'foka', 10);
INSERT INTO zwierze VALUES ('Sasza', 'foka', 15);


INSERT INTO sponsor VALUES ('Jacek', 'mors');
INSERT INTO sponsor VALUES ('Agatka', 'uchatka');
INSERT INTO sponsor VALUES ('Bolek', 'foka');
INSERT INTO sponsor VALUES ('Lolek', 'foka');
INSERT INTO sponsor VALUES ('Rumcajs', 'mors');


INSERT INTO datek VALUES ('Jacek', 'Gudrun', 10);
INSERT INTO datek VALUES ('Jacek', 'Pepe', 25);

INSERT INTO datek VALUES ('Agatka', 'Pepe', 20);
INSERT INTO datek VALUES ('Agatka', 'Gudrun', 10);
INSERT INTO datek VALUES ('Agatka', 'Dolores', 20);

INSERT INTO datek VALUES ('Bolek', 'Masza', 10);
INSERT INTO datek VALUES ('Bolek', 'Sasza', 15);

COMMIT;







